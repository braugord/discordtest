const mongoose = require('mongoose');

let characterSchema = new mongoose.Schema({  name : String,
                                    race : String,
                                    level : Number
});

module.exports = mongoose.model('CharacterModel', characterSchema, 'Collection0');

