var commands = ['one', 'two', 'three'];
// Testing random stuff for discord reply

module.exports =
{
	CheckCommands : function(msg)
	{
		if(msg.content === 'commands')
  		{
  			msg.reply(commands);
  		}
  		else
  		{
			for(i=0; i<commands.length; i++)
	  		{
	  			if(msg.content === commands[i])
	  			{
	  				msg.reply(commands[i]);
	  			}
	  		}
	  	}
	} ,

	GetSourceCode : function(msg)
	{
		msg.channel.send({
  files: [{
    attachment: './commands.js',
    name: 'commands.js'
  }]
})
  .then(console.log)
  .catch(console.error);
	} ,

	Viking : function(msg)
	{
		// Testing to send a remote file
		msg.channel.send({
		  files: ['https://i.imgur.com/Je6aiXo.jpg?size=2048']
		})
		  .then(console.log)
		  .catch(console.error);
	}
}