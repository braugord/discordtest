
const database = require('./mariadb_test_database');
const commands = require('./commands.js');
const config = require('./config.js');

const discord = require('discord.js');
const client = new discord.Client();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.login(config.discord_token);

client.on('message', msg => 
{
  console.log(msg.channel.name);
  if(msg.channel.name === 'testbotchannel')
  {
    let split = msg.content.split(' ');
  	if (msg.content === 'ping') 
    {
      msg.delete(msg);
      console.log(msg.guild.id);
      msg.reply('pang1');


     
    }
    if (split[0] === '!create') 
    {
      database.CreateUser(msg);
    }
    if(split[0] === '!char')
    {
      database.GetCharacter(msg);
    }
    if(split[0] === '!combat')
    {
      database.Combat(msg);
    }
    if (msg.content === 'SourceCode') 
    {
      commands.GetSourceCode(msg);
    }
    if (msg.content === 'Viking') 
    {
      commands.Viking(msg);
    }
    commands.CheckCommands(msg);
  } 
});

