const mongoose = require('mongoose');
const config = require('./config.js');

const server = config.mongoose_server;
const database = config.mongoose_database   
const options = config.mongoose_options;

console.log(`Connecting to: mongodb+srv://${server}/${database}${options}`);

class Database {
  constructor() {
    this._connect()
  }
  
_connect() {
     mongoose.connect(`mongodb+srv://${server}/${database}${options}`, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
       .then(() => {
         console.log('Database connection successful')
       })
       .catch(err => {
         console.error('Database connection error')
       })
  }
}

module.exports = new Database()