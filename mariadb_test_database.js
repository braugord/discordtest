const mariadb = require('mariadb');
const Discord = require('discord.js');
const config = require('./config.js');

const pool = mariadb.createPool({
    host: config.mariadb_host, 
    user:  config.mariadb_user, 
    password: config.mariadb_password,
    database: config.mariadb_database,
    connectionLimit: 5});

class Database 
{
    constructor() 
    {
    }
    
    async CreateUser(msg) 
    {
        let conn;
        try 
        {
            let discordID = msg.author.id;
            let guildID = msg.guild.id;
            conn = await pool.getConnection();
            let query = "SELECT UserID FROM Users WHERE DiscordID=? AND GuildID=?";
            let result = await conn.query(query, [discordID, guildID] );
            if(result.length == 0) // No used exists
            {
                console.log("User did not exist, creating user");
                query = "INSERT INTO Users SET DiscordID=?, GuildID=?";
                result = await conn.query(query, [discordID, guildID]);
                console.log(result);
            }  
            else // User already exist
            {
                console.log("User exists");
                console.log(result);
            }
        } 
        catch (err) { throw err; } 
        finally {   if (conn) conn.release(); /*release to pool*/  }
    }

    async GetCharacter(msg)
    {
        let conn;
        try 
        {
            let discordID = msg.author.id;
            let guildID = msg.guild.id;
            conn = await pool.getConnection();
            let query = "SELECT UserID FROM Users WHERE DiscordID=? AND GuildID=? LIMIT 1";
            let result = await conn.query(query, [discordID, guildID] );
            console.log(result);
            if(result.length == 0) // No user exists
            {
                console.log("no user exists");
            }  
            else // User already exist
            {
                console.log("user exists");
                console.log(result[0].UserID);
                query = "SELECT * FROM Characters WHERE UserID=? LIMIT 1";
                result = await conn.query(query, [result[0].UserID]);
                console.log("User exists");
                console.log(result[0]);

                const exampleEmbed = new Discord.MessageEmbed()
                .setColor('#0099ff')
                .setTitle('Character Screen')
                .setAuthor(result[0].Name, 'https://i.imgur.com/wSTFkRM.png')
                .setDescription('Description')
                .addFields(
                    { name: 'Race', value: result[0].Race, inline : true },
                    { name: 'Class', value: result[0].Class, inline: true },
                    { name: 'Level', value: result[0].Level, inline: true },
                )
                .setTimestamp()
                msg.channel.send(exampleEmbed);


            }
        } 
        catch (err) { throw err; } 
        finally {   if (conn) conn.release(); /*release to pool*/  }
    }

    async Combat(msg)
    {
        let conn;
        try 
        {
            let discordID = msg.author.id;
            let guildID = msg.guild.id;
            conn = await pool.getConnection();
            let query = "SELECT UserID FROM Users WHERE DiscordID=? AND GuildID=? LIMIT 1";
            let result = await conn.query(query, [discordID, guildID] );
            console.log(result);
            if(result.length == 0) // No user exists
            {
                console.log("no user exists");
            }  
            else // User already exist
            {
                console.log("user exists");
                console.log(result[0].UserID);
                query = "SELECT * FROM Characters WHERE UserID=? LIMIT 1";
                result = await conn.query(query, [result[0].UserID]);
                console.log("User exists");
                console.log(result[0]);

                let goblinhp = 20;
                let userhp = 20;
                let turn = true;
                let attacks = [];
                while(goblinhp > 0 && userhp > 0)
                {
                    if(turn)
                    {
                        let dmg = Math.floor(Math.random() * 10);
                        userhp -= dmg;
                        attacks.push({text:"Goblin attacks for " + dmg + " damage.", value : "Player has " + userhp + " left." });
                    }
                    else
                    {
                        let dmg = Math.floor(Math.random() * 10);  
                        goblinhp -= dmg;
                        attacks.push({text:"Player attacks for " + dmg + " damage.", value : "Goblin has " + userhp + " left." });  
                    }
                    turn = !turn;
                }
                if(goblinhp <= 0)
                {
                    attacks.push({text:"The goblin dies!", value : "Player earns 20 experience." });
                }
                if(userhp <= 0)
                {
                    attacks.push({text:"The player is killed!", value : "Mwop Mwop Mwop Mowoooooooooooooooow." });
                }
                console.log(attacks);
                const exampleEmbed = new Discord.MessageEmbed()
                .setColor('#882222')
                .setTitle('COMBAT!')
                .setAuthor(result[0].Name, 'https://hotemoji.com/images/emoji/l/t0h5iook8mql.png')
                .setDescription('A Goblin attacks from the shadows!')

                attacks.forEach(element => {
                    console.log(element);
                    console.log(element[0]);
                    let text = element['text'];
                    let value = element['value']
                    console.log(typeof(text));
                    console.log(typeof(value));
                    
                    exampleEmbed.addField(text, value, false);
                });
                
                msg.channel.send(exampleEmbed);


            }
        } 
        catch (err) { throw err; } 
        finally {   if (conn) conn.release(); /*release to pool*/  }
    }
}

module.exports = new Database();
