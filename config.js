// config.js
const dotenv = require('dotenv');

dotenv.config();

module.exports = {
    // MARIA_DB
    mariadb_host : process.env.MARIADB_HOST, 
    mariadb_user : process.env.MARIADB_USER, 
    mariadb_password : process.env.MARIADB_PASSWORD,
    mariadb_database : process.env.MARIADB_DATABASE,

    // DISCORD
    discord_token : process.env.DISCORD_TOKEN,

    // MONGOOSE
    mongoose_server : process.env.MONGOOSE_SERVER,
    mongoose_database : process.env.MONGOOSE_DATABASE,
    mongoose_options : process.env.MONGOOSE_OPTIONS,

    // MYSQL
    mysql_host : process.env.MYSQL_HOST,
    mysql_user : process.env.MYSQL_USER,
    mysql_password : process.env.MYSQL_PASSWORD,
    mysql_database : process.env.MYSQL_DATABASE
};